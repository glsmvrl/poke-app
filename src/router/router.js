import Vue from 'vue';
import VueRouter from 'vue-router';
import PokemonDetail from '@/views/PokemonDetail';
import HomePage from '@/views/HomePage';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home Page',
    component: HomePage,
  },
  {
    path: '/:name/:id',
    name: 'Pokemon Details',
    component: PokemonDetail,
    props: true,
  },
];

const router = new VueRouter({
  mode: 'history',
  routes,
});

export default router;
