import Vue from 'vue';
import Vuex from 'vuex';
import Service from '@/services/service';

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    noResultMessage: '',
    searchPokemonResult: [],
    pokemonsInformation: [],
    count: [],
    pokemonSpecies: [],
  },
  mutations: {
    SET_SEARCH_POKEMON(state, payload) {
      state.searchPokemonResult = payload;
    },
    SET_POKEMONS(state, payload) {
      state.pokemonsInformation = payload;
    },
    NO_RESULT_MESSAGE(state, payload) {
      state.noResultMessage = payload;
    },
    SET_COUNT(state, payload) {
      state.count = payload;
    },
    SET_POKEMON_SPECIES(state, payload) {
      state.pokemonSpecies = payload;
    },
  },
  actions: {
    async searchPokemon({ commit }, payload) {
      try {
        const response = await Service.searchPokemon(payload);
        const { data } = response;
        const convertToArray = [];
        convertToArray.push(data);
        const searchPokemon = convertToArray.map((result) => {
          const {
            id, name, height, weight, stats,
          } = result;
          const image = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${id}.png`;
          const types = Object.values(result.types.map((item) => item.type.name));
          const abilities = Object.values(result.abilities.map((item) => item.ability.name));
          return {
            name,
            id,
            image,
            height,
            weight,
            types,
            abilities,
            stats,
          };
        });
        commit('SET_SEARCH_POKEMON', searchPokemon);
      } catch (err) {
        const message = 'There were no result.';
        commit('NO_RESULT_MESSAGE', message);
        console.error(err);
      }
    },
    async fetchPokemons({ commit }, payload) {
      try {
        const { page } = payload;
        const { perPage } = payload;
        const offset = (page * perPage) - perPage;
        const response = await Service.fetchPokemons(offset);
        const { data } = response;
        const { count } = data;
        commit('SET_COUNT', count);
        const { results } = data;
        const mappedResults = results.map((res) => {
          const { name, url } = res;
          const id = url.split('https://pokeapi.co/api/v2/pokemon')[1].replaceAll('/', '');
          const image = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${id}.png`;
          return {
            name,
            id,
            image,
          };
        });
        commit('SET_POKEMONS', mappedResults);
      } catch (err) {
        const message = 'There were no result.';
        commit('NO_RESULT_MESSAGE', message);
        console.error(err);
      }
    },
    async fetchPokemonSpecies({ commit }, payload) {
      const response = await Service.fetchPokemonSpecies(payload);
      const { data } = response;
      const convertToArray = [];
      convertToArray.push(data);
      commit('SET_POKEMON_SPECIES', convertToArray);
    },
  },
  getters: {
    searchPokemonLength(state) {
      return state.searchPokemon.length;
    },
  },
});
