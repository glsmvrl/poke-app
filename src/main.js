import Vue from 'vue';
import router from '@/router/router';
import App from '@/App.vue';
import { store } from '@/store/store';
import '@/index.css';
import '@/styles/main.scss';

Vue.config.productionTip = false;

new Vue({
  render: (h) => h(App),
  router,
  store,
}).$mount('#app');
