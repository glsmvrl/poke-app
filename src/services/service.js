import axios from 'axios';

const config = {
  baseURL: process.env.VUE_APP_POKE_API,
};

const limit = 24;

class Service {
  constructor() {
    this.api = axios.create(config);
  }

  fetchPokemons(offset) {
    return this.api.get(`/pokemon?limit=${limit}&offset=${offset}`);
  }

  searchPokemon(payload) {
    return this.api.get(`/pokemon/${payload}`);
  }

  fetchPokemonSpecies(payload) {
    return this.api.get(`/pokemon-species/${payload}`);
  }
}

export default new Service();
